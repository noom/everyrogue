pub mod color {
    use termion::color::Rgb;

    pub const WHITE: Rgb = Rgb(200, 200, 200);
    pub const RED: Rgb = Rgb(255, 0, 0);
    pub const PURPLE: Rgb = Rgb(127, 0, 255);
}

pub mod symbol {
    pub const FULL: char = '\u{2588}';
    pub const LOWER_HALF: char = '\u{2584}';
    pub const UPPER_HALF: char = '\u{2580}';
    pub const SWORD: char = '\u{2694}';
}

pub enum Rect {
    Simple,
    Fine,
}
