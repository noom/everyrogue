use termion::{clear, color, cursor};

use std::fs::File;
use std::io::Read;

use crate::draw;
use crate::player::Player;
use crate::style;

pub struct Map {
    map: Vec<String>,
    map_size: (i32, i32),

    screen_offset: (u16, u16),
    screen_size: (u16, u16),

    player: Player,
}

impl Map {
    pub fn new(map_name: &str) -> Self {
        let mut file = File::open(map_name).unwrap();
        let mut content = String::new();

        file.read_to_string(&mut content).unwrap();
        let map: Vec<String> = content.split_whitespace().map(|x| x.to_owned()).collect();
        let map_size = (map[0].len() as i32, map.len() as i32);

        Self {
            map,
            map_size,
            screen_offset: (5, 3),
            screen_size: (21, 21),
            player: Player::new(),
        }
    }

    pub fn render(&mut self) {
        let mut buffer = String::new();

        let map_offset = self.calculate_map_offset();

        let mut index: (i32, i32) = (-1, -1);
        for line in &self.map {
            index.1 += 1;

            if index.1 - map_offset.1.abs() > self.screen_size.1 as i32 - 1 {
                break;
            }

            if index.1 < map_offset.1.abs() {
                continue;
            }

            let mut last_ch: char = '\0';
            for ch in line.chars() {
                index.0 += 1;

                if index.0 - map_offset.0.abs() > self.screen_size.0 as i32 - 1 {
                    break;
                }

                if index.0 < map_offset.0.abs() {
                    buffer = format!("{}", buffer);
                    continue;
                }

                if last_ch == '\0' || ch != last_ch {
                    buffer = match ch {
                        ',' => format!("{}{}{}", buffer, color::Fg(color::Green), ch),
                        _ => format!("{}{}{}", buffer, color::Fg(color::Reset), ch),
                    };
                } else {
                    buffer = format!("{}{}", buffer, ch);
                }
                last_ch = ch;
            }

            index.0 = -1;
            buffer = format!("{}{}", buffer, self.newline());
        }

        print!(
            "{}{}",
            cursor::Goto(self.screen_offset.0 + 1, self.screen_offset.1 + 1),
            buffer
        );

        print!(
            "{}@\n",
            cursor::Goto(
                self.player.pos.0 + self.screen_offset.0 - map_offset.0.abs() as u16 + 1,
                self.player.pos.1 + self.screen_offset.1 - map_offset.1.abs() as u16 + 1
            )
        );

        draw::rect(
            (self.screen_offset.0 - 1, self.screen_offset.1),
            (self.screen_size.0 + 4, self.screen_size.1 + 2),
            style::Rect::Fine,
            style::color::PURPLE,
        );
    }

    fn calculate_map_offset(&self) -> (i32, i32) {
        (
            self.screen_size.0 as i32 / 2 - self.player.pos.0 as i32,
            self.screen_size.1 as i32 / 2 - self.player.pos.1 as i32,
        )
    }

    fn newline(&self) -> String {
        if self.screen_offset.0 > 1 {
            format!(
                "{}{}{}",
                cursor::Down(1),
                cursor::Left(100),
                cursor::Right(self.screen_offset.0)
            )
        } else {
            format!("{}{}", cursor::Down(1), cursor::Left(100))
        }
    }
}
