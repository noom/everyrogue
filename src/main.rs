use std::io;
use std::io::Read;

use termion::raw::IntoRawMode;

mod draw;
mod map;
mod player;
mod style;

use crate::map::Map;

fn main() {
    let window_size = termion::terminal_size().expect("Cannot get terminal size.");

    print!("{}{}", termion::clear::All, termion::cursor::Hide);

    //     draw::rect(
    //         1,
    //         1,
    //         window_size.0 - 1,
    //         window_size.1 - 1,
    //         style::Rect::Fine,
    //         style::color::PURPLE,
    //     );

    let mut map = Map::new("res/map");

    let mut input = String::new();

    loop {
        io::stdin().read_line(&mut input).unwrap();
        map.render();
    }

    println!("Bye!{}", termion::cursor::Show);
}
