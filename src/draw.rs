use std::io::{stdin, stdout, Write};
use termion::raw::IntoRawMode;

use termion::{color, cursor};

use crate::style::{symbol, Rect};

pub fn rect(xy: (u16, u16), wh: (u16, u16), style: Rect, fg: color::Rgb) {
    let mut buffer = String::new();
    buffer = format!("{}{}", color::Fg(fg), cursor::Goto(xy.0, xy.1));

    for _ in xy.0..xy.0 + wh.0 {
        buffer = match style {
            Rect::Simple => format!("{}{}", buffer, symbol::FULL),
            Rect::Fine => format!("{}{}", buffer, symbol::LOWER_HALF),
        }
    }
    for _ in xy.1..xy.1 + wh.1 - 1 {
        buffer = format!("{}{}{}", buffer, cursor::Down(1), cursor::Left(1));
        buffer = format!("{}{}", buffer, symbol::FULL);
    }

    buffer = format!("{}{}", buffer, cursor::Goto(xy.0, xy.1));
    for i in xy.1..xy.1 + wh.1 - 1 {
        if i == xy.1 {
            buffer = format!("{}{}", buffer, cursor::Down(1));
        } else {
            buffer = format!("{}{}{}", buffer, cursor::Down(1), cursor::Left(1));
        }
        buffer = format!("{}{}", buffer, symbol::FULL);
    }
    // buffer = format!("{}{}", buffer, cursor::Left(2));
    for _ in xy.0..xy.0 + wh.0 - 2 {
        buffer = match style {
            Rect::Simple => format!("{}{}", buffer, symbol::FULL),
            Rect::Fine => format!("{}{}", buffer, symbol::LOWER_HALF),
            // Rect::Fine => format!("{}{}", buffer, symbol::UPPER_HALF),
        }
    }
    // println!("{}", buffer);

    buffer = format!("{}{}", buffer, cursor::Goto(1, 1));
    let mut stdout = std::io::stdout().into_raw_mode().unwrap();
    write!(stdout, "{}", buffer).unwrap();

    //     match style {
    //         Rect::Simple => {
    //             for _ in xy.0..xy.0 + w {
    //                 buffer = format!("{}{}", buffer, symbol::FULL);
    //             }
    //             for _ in y..y + h - 1 {
    //                 buffer = format!("{}{}{}", buffer, cursor::Down(1), cursor::Left(1));
    //                 buffer = format!("{}{}", buffer, symbol::FULL);
    //             }

    //             buffer = format!("{}{}", buffer, cursor::Goto(xy.0, y));
    //             for i in y..y + h - 1 {
    //                 if i == y {
    //                     buffer = format!("{}{}", buffer, cursor::Down(1));
    //                 } else {
    //                     buffer = format!("{}{}{}", buffer, cursor::Down(1), cursor::Left(1));
    //                 }
    //                 buffer = format!("{}{}", buffer, symbol::FULL);
    //             }
    //             for _ in xy.0..xy.0 + w - 2 {
    //                 buffer = format!("{}{}", buffer, symbol::FULL);
    //             }
    //         }

    //         // TODO: manager the border with LOWER and UPPER half
    //         Rect::Fine => {
    //             for _ in xy.0..xy.0 + w {
    //                 buffer = format!("{}{}", buffer, symbol::LOWER_HALF);
    //             }
    //             for _ in y..y + h - 1 {
    //                 buffer = format!("{}{}{}", buffer, cursor::Down(1), cursor::Left(1));
    //                 buffer = format!("{}{}", buffer, symbol::FULL);
    //             }

    //             buffer = format!("{}{}", buffer, cursor::Goto(xy.0, y));
    //             for i in y..y + h - 1 {
    //                 if i == y {
    //                     buffer = format!("{}{}", buffer, cursor::Down(1));
    //                 } else {
    //                     buffer = format!("{}{}{}", buffer, cursor::Down(1), cursor::Left(1));
    //                 }
    //                 buffer = format!("{}{}", buffer, symbol::FULL);
    //             }
    //             for _ in xy.0..xy.0 + w - 2 {
    //                 buffer = format!("{}{}", buffer, symbol::LOWER_HALF);
    //             }
    //         }
    //     }
}
